# -*- encoding: utf-8 -*-
########################################################################
#
# @authors: Jairo Troncoso
# Copyright (C) 2013  Electrom 
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
# This module is GPLv3 or newer and incompatible
# with OpenERP SA "AGPL + Private Use License"!
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see http://www.gnu.org/licenses.
########################################################################
from osv import osv
from osv import fields
import decimal_precision as dp
import netsvc
from tools.translate import _
import time
from openerp import tools

class email_template(osv.osv):
    '''
    Open ERP Model
    '''
    _inherit = 'email.template'

    def generate_email(self, cr, uid, template_id, res_id, context=None):
        """Generates an email from the template for given (model, res_id) pair.

           :param template_id: id of the template to render.
           :param res_id: id of the record to use for rendering the template (model
                          is taken from template definition)
           :returns: a dict containing all relevant fields for creating a new
                     mail.mail entry, with one extra key ``attachments``, in the
                     format expected by :py:meth:`mail_thread.message_post`.
        """
        valid_attachment_ids =[]
        doc_direct_obj = self.pool.get("document.directory")
        if context is None:
            context = {}
        cisc_util_obj = self.pool.get('cisc.utils')
        report_xml_pool = self.pool.get('ir.actions.report.xml')
        template = self.get_email_template(cr, uid, template_id, res_id, context)
        values = {}
        for field in ['subject', 'body_html', 'email_from',
                      'email_to', 'email_recipients', 'email_cc', 'reply_to']:
            values[field] = self.render_template(cr, uid, getattr(template, field),
                                                 template.model, res_id, context=context) \
                                                 or False
        if template.user_signature:
            signature = self.pool.get('res.users').browse(cr, uid, uid, context).signature
            values['body_html'] = tools.append_content_to_html(values['body_html'], signature)

        if values['body_html']:
            values['body'] = tools.html_sanitize(values['body_html'])

        values.update(mail_server_id=template.mail_server_id.id or False,
                      auto_delete=template.auto_delete,
                      model=template.model,
                      res_id=res_id or False)

        attachments = []
        reports = []
        # Info Factura
        if context.get('active_model') =='account.invoice.ext':
            attachment_ids = []
            invoice = self.pool.get('account.invoice.ext').browse(cr, uid, context['active_id'])
            model_report ='account.invoice.ext'
            valid_attachment_ids = self.pool.get('ir.attachment').search(cr, uid, [('document_electronic_authorization','=',True), ('res_model','=', model_report), ('res_id', '=', invoice.id), ('file_type','=','application/pdf')])
            if not valid_attachment_ids:
                cr.execute('delete from ir_attachment where res_model=%s and res_id=%s and file_type=%s' ,(model_report,tuple([invoice.id]),'application/pdf'))
                ids_report = res_id
                if context.get('type') == 'factura':
                    name_report = 'RIDE-Factura'
                    name = 'RIDE FACT '+invoice.number
                elif context.get('type') == 'ncredito':
                    name_report = 'RIDE-Nota credito'
                    name = 'RIDE NC '+invoice.number
                else:
                    name_report = 'RIDE-Nota debito'
                    name = 'RIDE ND '+invoice.number
                
                self.pool.get('account.invoice.ext').imprimirReporte(cr, uid, [res_id], context=context)
                service = netsvc.LocalService('report.reportjasper_'+context.get('type'))
                (result, format) = service.create(cr, uid, [res_id], {'model':'account.invoice.ext','id':res_id, 'type':context.get('type'), 'jasper':{'id':res_id}}, context=context)
                report_file = name_report +" "+ invoice.number+"."+format
                self.pool.get('ir.attachment').create(cr, uid, {'name':report_file,'datas_fname':report_file, 'datas':result.encode('base64'), 'res_id':res_id, 'res_model':'account.invoice.ext','document_electronic':True, 'document_electronic_authorization':True,'file_type':'application/pdf'})
                
            #reports.append(cisc_util_obj.create_report(cr, uid, ids_report, name_report, model_report,  name, context)[0])
            attachment_ids = self.pool.get('ir.attachment').search(cr, uid, ['|',('document_electronic','=',True),('document_electronic_authorization','=',True),  ('res_model','=', model_report), ('res_id', '=', invoice.id)])
            if attachment_ids:
                values['attachment_ids'] = attachment_ids
            
            
        elif context.get('active_model') =='account.retention.ext':
            # Add report in attachments}
            retention = self.pool.get('account.retention.ext').browse(cr, uid, context['active_id'])
            model_report ='account.retention.ext'
            attachment_ids = []
            #if template.report_template or context.get('report') in ('retention'):
            ids_report = res_id
            
            valid_attachment_ids = self.pool.get('ir.attachment').search(cr, uid, [('document_electronic_authorization','=',True), ('res_model','=', model_report), ('res_id', '=', retention.id), ('file_type','=','application/pdf')])
            if not valid_attachment_ids:
            #if context.get('report') =='retention':
                cr.execute('delete from ir_attachment where res_model=%s and res_id=%s and file_type=%s' ,(model_report,tuple([retention.id]),'application/pdf'))
                name_report = 'RIDE-Retencion'
                name = 'RIDE RET '+ retention.number
                #reports.append(cisc_util_obj.create_report(cr, uid, ids_report, name_report, model_report,  name, context)[0])
                self.pool.get('account.retention.ext').imprimirReporte(cr, uid, [res_id], context=context)
                service = netsvc.LocalService('report.reportjasper_retention')
                (result, format) = service.create(cr, uid, [res_id], {'model':model_report,'id':res_id, 'jasper':{'id':res_id}}, context=context)
                report_file = name_report+" " + retention.number+"."+format
                self.pool.get('ir.attachment').create(cr, uid, {'name':report_file,'datas_fname':report_file, 'datas':result.encode('base64'),'res_id':res_id, 'res_model':'account.retention.ext','document_electronic':True, 'document_electronic_authorization':True,'file_type':'application/pdf'})
            attachment_ids = self.pool.get('ir.attachment').search(cr, uid, ['|',('document_electronic','=',True),('document_electronic_authorization','=',True), ('res_model','=', model_report), ('res_id', '=', retention.id)])    
            # Add template attachments
            if attachment_ids:                
                values['attachment_ids'] = attachment_ids   
                         
        elif context.get('active_model') =='remission.guide.ext':
            remision = self.pool.get('remission.guide.ext').browse(cr, uid, context['active_id'])
            model_report ='remission.guide.ext'
            attachment_ids = []
            #if template.report_template or context.get('report') in ('retention'):
            ids_report = res_id
            
            valid_attachment_ids = self.pool.get('ir.attachment').search(cr, uid, [('document_electronic_authorization','=',True), ('res_model','=', model_report), ('res_id', '=', remision.id), ('file_type','=','application/pdf')])
            if not valid_attachment_ids:
            #if context.get('report') =='retention':
                cr.execute('delete from ir_attachment where res_model=%s and res_id=%s and file_type=%s' ,(model_report,tuple([remision.id]),'application/pdf'))
                name_report = 'RIDE-Guia remision'
                name = 'RIDE GUIA '+ remision.number
                model_report ='remission.guide.ext'  
                self.pool.get('remission.guide.ext').imprimirReporte(cr, uid, [res_id], context=context)
                service = netsvc.LocalService('report.reportjasper_remission')
                (result, format) = service.create(cr, uid, [res_id], {'model':model_report,'id':res_id, 'jasper':{'id':res_id}}, context=context)
                report_file = name_report+" " + remision.number+".pdf"
                self.pool.get('ir.attachment').create(cr, uid, {'name':report_file,'datas_fname':report_file, 'datas':result.encode('base64'), 'res_id':res_id, 'res_model':'remission.guide.ext','document_electronic':True, 'document_electronic_authorization':True,'file_type':'application/pdf'})
            attachment_ids = self.pool.get('ir.attachment').search(cr, uid, ['|',('document_electronic','=',True),('document_electronic_authorization','=',True), ('res_model','=', model_report), ('res_id', '=', remision.id)])    
            # Add template attachments
            if attachment_ids:
                values['attachment_ids'] = attachment_ids
        return values
email_template()