# -*- encoding: utf-8 -*-
########################################################################
#
# @authors: Jairo Troncoso
# Copyright (C) 2014  CISC
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
# This module is GPLv3 or newer and incompatible
# with OpenERP SA "AGPL + Private Use License"!
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see http://www.gnu.org/licenses.
########################################################################

from osv import osv
from osv import fields
from tools.translate import _
import jasper_reports


def reportjasper_document( cr, uid, ids, data, context ):
    return {
        'parameters': {    
            'id': data['id'],
        }
   }


class account_retention_ext(osv.osv):
    _inherit = "account.retention.ext"    
    
    _columns={
                  'number':fields.char('Número comprobante', size=17, readonly=True),
                  'partner_id':fields.many2one('res.partner', 'Cliente', readonly=True),
                  'user_id':fields.many2one('res.users', 'Usuario', required=False),
                  'company_id':fields.many2one('res.company', 'Compañia', required=False),
                  'retention_line_ext': fields.one2many('account.retention.line.ext', 'retention_ext_id', 'Detalles', states={'authorized':[('readonly', True)],'key_contingency':[('readonly',True)]}),
                  'name_file':fields.char('nombre_archivo', size=64, readonly=True),
                  'fechaemision': fields.date('Fecha emisión'),
                  'direstablecimiento': fields.char('Dirección', states={'authorized':[('readonly', True)],'key_contingency':[('readonly',True)]}, size=128),
                  'identificacionsujetoretenido': fields.char('RUC / CI', states={'authorized':[('readonly', True)],'key_contingency':[('readonly',True)]}, size=64),
                  'periodofiscal':fields.char('Periodo fiscal', size=30, states={'authorized':[('readonly', True)],'key_contingency':[('readonly',True)]}),  
                  'note': fields.text('Otra información'),
                  'partner_name':fields.char('Cliente',size=128, states={'authorized':[('readonly', True)],'key_contingency':[('readonly',True)]}),
                  'partner_email':fields.char('email',size=128, states={'authorized':[('readonly', True)],'key_contingency':[('readonly',True)]}),
                  'type_ref':fields.char('type_ref', size=5),
        'cod_sustento': fields.char('Cod. sustento'),
        'cod_doc_sustento': fields.char('Cod. doc. sustento'),
        'num_doc_sustento': fields.char('Num doc sustento'),
        'fecha_emision_doc_sustento': fields.char('Fecha emision doc sustento'),
        'pago_loc': fields.char('Pago Loc'),
        'pago_reg_fis': fields.char('Pago reg fis'),
        'tipo_regi': fields.char('Tipo Regi'),
        'pais_efec_pago': fields.char('Pais efec pago'),
        'aplic_conv': fields.char('Aplic_COnv'),
        'tipo_sujeto_retenido': fields.char('Tipo sujeto retenido'),
        # 'total_reembolso': fields.float('Total reembolso'),
        # 'total_base_imponible_reembolso': fields.float('Total base reembolso'),
        # 'total_impuesto_reembolso': fields.float('Total impuesto reembolso'),
        'total_sin_impuesto': fields.float('Total impuesto'),
        'importe_total': fields.float('Total'),
        'base_0': fields.float('Base 0'),
        'base_iva': fields.float('Base iva'),
        'base_iva_5': fields.float('Base iva 5'),
        'iva': fields.float('Iva'),
        'codigo_pago': fields.char('Codigo pago'),
        'porcentaje_iva': fields.char('Porcentaje iva'),
        'codigo_porcentaje_iva': fields.char('Codigo porcentaje iva'),
              }
    _order = "number desc"
    _rec_name = 'number'
    
    _defaults = {
        'company_id': lambda self,cr,uid,c: self.pool.get('res.users').browse(cr, uid, uid, c).company_id.id,
        'state': 'loaded'
            }
    
    
    def unlink(self, cr, uid, ids, context=None):
        if not context:
            context={}
        for retention in self.browse(cr , uid ,ids):
            if retention.state in ('authorized', 'received', 'key_contingency'):
                raise osv.except_osv(_('Error!'), _("No puedes eliminar comprobantes que ya esten autorizados, que esten esperando autorización o que se emitan con claves de contingencia" ))
            docs = self.pool.get('ir.attachment').search(cr, uid ,[('res_id','=',retention.id),('res_model','=','account.retention.ext')])
            if docs:
                cr.execute('delete from ir_attachment where id IN %s', (tuple(docs),))
        res = super(account_retention_ext, self).unlink(cr, uid, ids, context)       
        return res
    
    def document_electronic_withhold_sri(self, cr, uid , ids , context=None):
        model = "account.retention.ext"
        type_document = "comprobanteRetencion"
        type_doc_dir = "Retenciones"
        compr = "07"
        for withh in self.browse(cr , uid , ids , context=None): 
            self.pool.get("electronic.invoicing.sri").validation_document_sri(cr , uid, [withh.id], model, type_document, compr, type_doc_dir, context=None)
        return True  
    
    def imprimirReporte(self, cr, uid, ids, context=None):
        data = {}
        data['model'] = 'account.retention.ext'
        data['id'] = ids[0]
        data['jasper'] = {
                       'id':ids[0]
                      }
        jasper_reports.report_jasper(
            'report.reportjasper_retention',
            'account.retention.ext',
            parser=reportjasper_document
        )
        
        return{
               'type':'ir.actions.report.xml',
               'report_name':'reportjasper_retention',
               'datas':data
               }
        
    def action_withhold_sent(self, cr, uid, ids, context=None):
        if not context: context= None
        assert len(ids) == 1, 'This option should only be used for a single id at a time.'
        ir_model_data = self.pool.get('ir.model.data')
        try:
            template_id = ir_model_data.get_object_reference(cr, uid, 'voucher_electronic', 'email_template_edi_withhold_ext')[1]
        except ValueError:
            template_id = False
        try:
            compose_form_id = ir_model_data.get_object_reference(cr, uid, 'mail', 'email_compose_message_wizard_form')[1]
        except ValueError:
            compose_form_id = False
        ctx = dict(context)
        ctx.update({
            'default_model': 'account.retention.ext',
            'default_res_id': ids[0],
            'default_use_template': bool(template_id),
            'default_template_id': template_id,
            'default_composition_mode': 'comment',
            'mark_invoice_as_sent': True,
            })
        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form_id, 'form')],
            'view_id': compose_form_id,
            'target': 'new',
            'context': ctx,
        }


class account_retention_line_ext(osv.osv):
    _name = "account.retention.line.ext"
    _columns={
              'retention_ext_id': fields.many2one('account.retention.ext', 'Retention Reference', ondelete='cascade', select=True), 
              'codigo':fields.char('codigo', size=1),
              'type_document':fields.char('Comprobante', size=300, readonly=True),
              'tax':fields.char('Impuesto', size=15, readonly=True),
              'codigoretencion':fields.char('Codigo retencion', size=6),
              'baseimponible': fields.float('Base imponible para retención', digits=(16, 2)),
              'valorretenido': fields.float('Valor retenido', digits=(16, 2), readonly = True),              
              'porcentajeretener':fields.float('Porcentaje retener', size=5),
              'numdocsustento':fields.char('Número doc.', size=15),
        'cod_sustento': fields.char('Codigo'),
        'fecha_doc_sustento': fields.date('Fecha')
              }
        
    _defaults = {
        'type_document': 'FACTURA',
            }
