# -*- encoding: utf-8 -*-
########################################################################
#
# @authors: Jairo Troncoso
# Copyright (C) 2013  Electrocom
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
# This module is GPLv3 or newer and incompatible
# with OpenERP SA "AGPL + Private Use License"!
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see http://www.gnu.org/licenses.
########################################################################
from osv import osv
from osv import fields
import decimal_precision as dp
from tools.translate import _
import time
from datetime import datetime
from dateutil.relativedelta import relativedelta
import md5
from suds.client import Client

class sms_client_ele(osv.osv):
    '''
    Open ERP Model
    '''
    _name = 'sms.client.ele'
    _description = 'sms.client.ele'

    _columns = {
            'url':fields.char('URL', size=255, required=True ,help="url de webservice"),
            'emisor':fields.char('Emisor', size=255, required=True),
            'password':fields.char('Contraseña', size=255, required=True),
            'users_id': fields.many2many('res.users',
            'res_sms_ele_group_rel', 'smsid', 'uid', 'Usuarios permitidos'),
        }
    
    def send_sms_erp(self, cr, uid , url ,emisor, password, phone, message,  context=None):
        if not context: context ={}
        date_due = (datetime.now() - relativedelta(hours=5)).strftime('%m/%d/%Y %H:%M')# Zona horaria local
        datemsj= date_due[0:10]
        hoursmsj = date_due[10:]
#         url = 'https://app.eclipsoft.com:9443/wsSMSEmpresarial/wscSMSEmp.asmx?wsdl'
        client = Client(url)     
        clsEmisor = client.factory.create('clsEmisor')
        clsEmisor.emServicio = 'CONTACTOSMS'
#         clsEmisor.emEmisor = "ELECTROCOM"
        clsEmisor.emEmisor = emisor
        clsEmisor.emLogin = "admin" 
#         clsEmisor.emPwd = "lctr@csms" 
        clsEmisor.emPwd = password 
        clsEmisor.emReferencia = "5789" 
        clsEmisor.emFechaEnv = datemsj
        clsEmisor.emHoraEnv = hoursmsj
        clsEmisor.emNombrePC = "PCNEW"
#         clsEmisor.emKey = md5.md5("CONTACTOSMS;csms@auto;ELECTROCOM;admin;lctr@csms;5789").hexdigest()
        clsEmisor.emKey = md5.md5("CONTACTOSMS;csms@auto;"+emisor+";admin;"+password+";5789").hexdigest()
        clsEmisor.emLimite = 0
        clsEmisor.emTotalMes = 0        
#         result = client.service.EnviarSMS(clsEmisor,'0981411541', 'Prueba de Envio SMS desde Open Erp')
        result = client.service.EnviarSMS(clsEmisor, phone, message)
        print client    
        if result.reNumErrores == 0:
            state = "mensaje enviado"
        else:
            state = "mensaje no enviado"
        self.pool.get("sms.client.ele.history").create(cr , uid ,
                                                          {
                                                           "state":state,
                                                           "phone":phone,
                                                           "message":message,
                                                           "date": date_due
                                                           })
        return True
    
    
sms_client_ele()

class sms_client_ele_history(osv.osv):
    '''
    Open ERP Model
    '''
    _name = 'sms.client.ele.history'
    _description = 'sms.client.ele.history'

    _columns = {
                'state':fields.char('Estado', size=64, readonly=True),
                'phone':fields.char('Movil', size=64, readonly=True),
                'message':fields.char('Mensaje', size=64, readonly=True), 
                'date':fields.char('Fecha', size=64, readonly=True), 
                }
    
sms_client_ele_history()

class partner_send_sms(osv.osv_memory):
    
    _name = 'partner.send.sms'
    _description = 'partner.send.sms'
    
    _columns = {
                'message': fields.text('Mensaje', required=True),
                'phone':fields.char('Movil', size=64, required=True),
                }
    
    def default_get(self, cr, uid, fields_list, context=None):
        if not context:
            context={}
        partner_pool = self.pool.get('res.partner')
        active_ids = context and context.get('active_ids', []) or []
        values = super(partner_send_sms, self).default_get(cr, uid, fields_list, context)
        for partner in partner_pool.browse(cr, uid, active_ids, context=context):         
            if not partner.mobile:
                break
            if partner.mobile[0:4] == '+593':
                values['phone'] = '0'+partner.mobile[4:]
            else:
               raise osv.except_osv(_(u'Advertencia'), _(u'El móvil del cliente %s no tiene el formato correcto , favor de verificar!!!')% partner.name) 
        return values
    
    
    def check_permissions(self, cr, uid, id, context=None):
        cr.execute('select * from res_sms_ele_group_rel where smsid=%s and uid=%s' % (id, uid))
        data = cr.fetchall()
        if len(data) <= 0:
            return False
        return True
    
    def send_message(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        webservice= self.pool.get('sms.client.ele')
        wiz = self.browse(cr ,uid , ids[0])
        message = wiz.message
        mss= message.encode('ascii', 'replace')
        gateway = webservice.search(cr , uid, [])
        if not gateway:
           raise osv.except_osv(_(u'Advertencia'), _(u'Debe configurar el URL webservices !!!')) 
        gt = webservice.browse(cr , uid ,gateway[0])
        if gateway:
            if not self.check_permissions(cr, uid, gt.id, context=context):
                raise osv.except_osv(_(u'Advertencia'), _(u'Usted no tiene permisos para enviar mensajes !!!'))
            else :
                webservice.send_sms_erp(cr, uid , str(gt.url) ,str(gt.emisor), str(gt.password), str(wiz.phone), mss,  context=None)
        return True
    
partner_send_sms()