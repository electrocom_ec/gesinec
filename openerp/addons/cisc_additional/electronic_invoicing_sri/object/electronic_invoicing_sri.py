# -*- encoding: utf-8 -*-
########################################################################
#
# @authors: Jairo Troncoso
# Copyright (C) 2014  CISC
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This module is GPLv3 or newer and incompatible
# with OpenERP SA "AGPL + Private Use License"!
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses.
########################################################################
from osv import osv
from osv import fields
from tools.translate import _
from xml.etree.ElementTree import Element, SubElement, tostring
import base64
from datetime import datetime

# variables globales
produccion = '2'
numbers = '0123456789'
create_date = datetime.now()


class res_users(osv.osv):
    _inherit = "res.users"

    def _get_state(self, cr, uid, ids, name, arg, context=None):
        res = {}
        for user in self.browse(cr, uid, ids, context):
            if user.login_date:
                res[user.id] = 'active'
            else:
                res[user.id] = 'new'
        return res

    _columns = {
        'notification': fields.boolean('Notificación fact. electrónica', required=False),
        'state': fields.function(_get_state, string='Status', type='selection',
                                 selection=[('new', 'Never Connected'), ('active', 'Activated')]),
        'is_system_create': fields.boolean('Creado por el sistema', required=False),
    }


res_users()


class webservice_sri(osv.osv):
    _name = 'webservice.sri'
    _description = 'webservice.sri'

    _columns = {
        'name': fields.char('Nombre', size=255, readonly=True),
    }


webservice_sri()


class res_partner(osv.osv):
    _inherit = "res.partner"

    _columns = {
        'is_mss': fields.boolean('Notificación mensaje de texto', required=False),
        'sent_invitation': fields.boolean('Invitacion enviada', required=False),
        'create_system': fields.boolean('Creado por el sistema', required=False)
    }

    def default_get(self, cr, uid, fields_list, context=None):
        if not context:
            context = {}
        values = super(res_partner, self).default_get(cr, uid, fields_list, context)
        if context.get('is_company'):
            values['is_company'] = True
        else:
            values['is_company'] = True
        return values


res_partner()


class ir_attachment(osv.osv):
    _inherit = "ir.attachment"

    _columns = {
        'document_electronic': fields.boolean('Documento electronico', required=False),
        'document_electronic_checked': fields.boolean('Documento electronico chequeado', required=False),
        'document_electronic_authorization': fields.boolean('Documento electronico autorizado', required=False),
    }

    def unlink(self, cr, uid, ids, context=None):
        if not context:
            context = {}
        for ir_atta in self.browse(cr, uid, ids):
            if ir_atta.document_electronic:
                raise osv.except_osv(_('Error!'),
                                     _("No puedes eliminar un documento electrónico ya que se encuentra ligado a una factura"))
        res = super(ir_attachment, self).unlink(cr, uid, ids, context)
        return res


ir_attachment()


class account_invoice_ext(osv.osv):
    _name = "account.invoice.ext"
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _columns = {
        'clave_acceso': fields.char('Clave acceso', size=64, readonly=True),
        'count': fields.integer('Contador'),
        'sent': fields.boolean('Enviado x correo', readonly=True),
        'electronic_authorization': fields.char('AUT. Electrónica', size=64, readonly=True),
        'state': fields.selection([
            ('loaded', 'Por Autorizar'),
            ('authorized', 'Autorizado'),
            ('unauthorized', 'No autorizado'),
        ], 'Estado', select=True, readonly=True),
        'motive': fields.text('Detalle de validación', readonly=True, help="Describe el motivo del rechazo"),
        'date_docu_authorization': fields.datetime('Fecha autorizacion', readonly=True),
        'type_broadcast': fields.selection([
            ('normal', 'Normal'),
        ], 'Tipo de Emisión', select=True, readonly=True),
        'web2_inactive': fields.boolean('Web 2 inactivo'),
        'date_docu_authorization_xmldb': fields.datetime('Fecha autorizacion', readonly=True),
        'observacion': fields.char('Observacion', size=1024, required=False, readonly=False),
        'ws': fields.integer('Webservice'),
    }


account_invoice_ext()


class account_retention_ext(osv.osv):
    _name = 'account.retention.ext'
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _columns = {
        'clave_acceso': fields.char('Clave acceso', size=64, readonly=True),
        'count': fields.integer('Contador'),
        'sent': fields.boolean('Enviado x correo', readonly=True),
        'electronic_authorization': fields.char('AUT. Electrónica', size=64, readonly=True),
        'state': fields.selection([
            ('loaded', 'Por Autorizar'),
            ('authorized', 'Autorizado'),
            ('unauthorized', 'No autorizado'),
        ], 'Estado', select=True, readonly=True),
        'motive': fields.text('Detalle de validación', readonly=True, help="Describe el motivo del rechazo"),
        'date_docu_authorization': fields.datetime('Fecha autorizacion'),
        'type_broadcast': fields.selection([
            ('normal', 'Normal'),
        ], 'Tipo de Emisión', select=True, readonly=True),
        'web2_inactive': fields.boolean('Web 2 inactivo'),
        'date_docu_authorization_xmldb': fields.datetime('Fecha autorizacion', readonly=True),
        'observacion': fields.char('Observacion', size=1024, required=False, readonly=False),
        'ws': fields.integer('Webservice'),
    }


class remission_guide_ext(osv.osv):
    _name = 'remission.guide.ext'
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _columns = {
        'clave_acceso': fields.char('Clave acceso', size=64, readonly=True),
        'count': fields.integer('Contador'),
        'sent': fields.boolean('Enviado x correo', readonly=True),
        'electronic_authorization': fields.char('AUT. Electrónica', size=64, readonly=True),
        'state': fields.selection([
            ('loaded', 'Por Autorizar'),
            ('authorized', 'Autorizado'),
            ('unauthorized', 'No autorizado'),
        ], 'Estado', select=True, readonly=True),
        'motive': fields.text('Detalle de validación', readonly=True, help="Describe el motivo del rechazo"),
        'date_docu_authorization': fields.datetime('Fecha autorizacion', readonly=True),
        'type_broadcast': fields.selection([
            ('normal', 'Normal'),
        ], 'Tipo de Emisión', select=True, readonly=True),
        'web2_inactive': fields.boolean('Web 2 inactivo'),
        'date_docu_authorization_xmldb': fields.datetime('Fecha autorizacion', readonly=True),
        'observacion': fields.char('Observacion', size=1024, required=False, readonly=False),
        'ws': fields.integer('Webservice'),
    }


class electronic_invoicing_sri(osv.osv):
    _name = "electronic.invoicing.sri"

    def name_retention(self, code):
        if code == '2':
            return 'IVA'
        elif code == '1':
            return 'RENTA'
        else:
            return ' '

    def tipo_identificacion(self, type):
        if type == 'ruc':
            return '04'
        elif type == 'cedula':
            return '05'
        elif type == 'passport':
            return '06'
        elif type == 'consumidor':
            return '07'

    def valtaxes(self, codigoPorcentaje, iva):
        if codigoPorcentaje in ('0', '6'):
            return 0.00
        return iva.percentage / 100.0

    def get_date_format(self, date):
        return date[2] + "/" + date[1] + "/" + date[0]

    def iva_percentage(self, pr):
        if pr == '1':
            return '30'
        elif pr == '2':
            return '70'
        elif pr == '3':
            return '100'
        elif pr == '9':
            return '10'
        elif pr == '10':
            return '20'
        elif pr == '7':
            return '0'

    def indent(self, elem, level=0, context=None):
        if not context: context = {}
        i = "\n" + level * "  "
        if len(elem):
            if not elem.text or not elem.text.strip():
                elem.text = i + "  "
                if level == 0 and not context.get('out_sri'):
                    elem.attrib["id"] = "comprobante"
                    elem.attrib["version"] = "1.0.0"
                if context.get('version'):
                    elem.attrib["version"] = context.get('version')
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
        for elem in elem:
            self.indent(elem, level + 1)
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
        else:
            if level and (not elem.tail or not elem.tail.strip()):
                elem.tail = i

    def generate_document_electronic_xml(
            self, cr, uid, ids, company, model, type_document, compr,
            file_contingency=None, context=None
            ):
        docu_elect = self.pool.get(model)
        partner_company = self.pool.get('res.company').read(cr, uid, company['id'], ['reason_social', 'ruc', 'street'])
        if not partner_company['street'] or not partner_company['ruc']:
            raise osv.except_osv(
                _('Error!'),
                _(u"Faltan datos importante en la ficha de la compañia tales como RUC y DIRECCION, favor de verificar.")
                )
        emision = '1'
        # XML FACTURA, NOTA CREDITO
        if type_document in ("notaCredito", "factura", "notaDebito"):
            invoice = docu_elect.browse(cr, uid, ids[0])
            est, pto, sec = invoice.number.split("-")
            anuario, mes, dia = invoice.fechaemision.split("-")
            clave_acceso = invoice.clave_acceso
            root = Element(type_document)
            tributaria = SubElement(root, "infoTributaria")
            SubElement(tributaria, "ambiente").text = produccion
            SubElement(tributaria, "tipoEmision").text = emision
            SubElement(tributaria, "razonSocial").text = partner_company['reason_social'].encode('ascii', 'ignore')
            SubElement(tributaria, "ruc").text = partner_company['ruc'].encode('ascii', 'ignore')
            SubElement(tributaria, "claveAcceso").text = clave_acceso
            SubElement(tributaria, "codDoc").text = compr
            SubElement(tributaria, "estab").text = est
            SubElement(tributaria, "ptoEmi").text = pto
            SubElement(tributaria, "secuencial").text = sec
            SubElement(tributaria, "dirMatriz").text = partner_company['street'].encode('ascii', 'ignore')
            SubElement(tributaria, "agenteRetencion").text = "1"
            if type_document == "factura":
                factura = SubElement(root, "infoFactura")
            elif type_document == "notaCredito":
                factura = SubElement(root, "infoNotaCredito")
            elif type_document == "notaDebito":
                factura = SubElement(root, "infoNotaDebito")
            SubElement(factura, "fechaEmision").text = dia + "/" + mes + "/" + anuario
            if invoice.partner_id.street:
                SubElement(factura, "dirEstablecimiento").text = partner_company['street'].encode('ascii', 'ignore')
            if type_document == "factura":
                # SubElement(factura,"contribuyenteEspecial").text= "1027"
                SubElement(factura, "obligadoContabilidad").text = "SI"
            SubElement(factura, "tipoIdentificacionComprador").text = invoice.partner_id.type_ref
            if invoice.guide_remision:
                SubElement(factura, "guiaRemision").text = invoice.guide_remision.strip()
            SubElement(factura, "razonSocialComprador").text = invoice.partner_id.name.encode('ascii', 'ignore')
            SubElement(factura, "identificacionComprador").text = invoice.partner_id.vat
            if type_document == 'factura':
                SubElement(factura, "direccionComprador").text = invoice.partner_id.street
            if type_document in ("notaCredito", "notaDebito"):
                SubElement(factura, "obligadoContabilidad").text = "SI"
                SubElement(factura, "codDocModificado").text = "01"
                SubElement(factura, "numDocModificado").text = str(invoice.numdocmodificado)
                anuario, mes, dia = invoice.fechaemisiondocsustento.split(" ")[0].split("-")
                SubElement(factura, "fechaEmisionDocSustento").text = dia + "/" + mes + "/" + anuario
            SubElement(factura, "totalSinImpuestos").text = str(round(invoice.totalsinimpuestos, 2))
            if type_document == "notaCredito":
                SubElement(factura, "valorModificacion").text = str(round(invoice.importetotal, 2))
            if type_document == "factura":
                SubElement(factura, "totalDescuento").text = str(invoice.totaldescuento)
                # datos reembolso
            if type_document == "factura" and invoice.is_reemb:
                SubElement(factura, "codDocReembolso").text = invoice.coddocreemb
                SubElement(factura, "totalComprobantesReembolso").text = invoice.totalcomprobantesreembolso
                SubElement(factura, "totalBaseImponibleReembolso").text = invoice.totalbaseimponiblereembolso
                SubElement(factura, "totalImpuestoReembolso").text = invoice.totalimpuestoreembolso

            if type_document in ("notaCredito", "factura"):
                if invoice.invoice_line_ext:
                    ttconimpuestos = SubElement(factura, "totalConImpuestos")
                    taxes = {}
                    for acu_taxe in invoice.invoice_line_ext:
                        code = acu_taxe.codigoporcentaje
                        iva = self._get_value_iva(cr, uid, acu_taxe.codigoporcentaje, context)
                        if code not in taxes:
                            taxes[code] = {
                                "iva": 0.0,
                                "subtotal": 0.0,
                                "code": acu_taxe.codigoporcentaje
                                }
                        taxes[code]["iva"] += acu_taxe.preciototalsinimpuesto * int(iva) / 100.0
                        taxes[code]["subtotal"] += acu_taxe.preciototalsinimpuesto
                    for tax in taxes:
                        ttimpuesto = SubElement(ttconimpuestos, "totalImpuesto")
                        SubElement(ttimpuesto, "codigo").text = '2'  # impuesto iva
                        SubElement(ttimpuesto, "codigoPorcentaje").text = taxes[tax]['code']
                        SubElement(ttimpuesto, "baseImponible").text = str(round(taxes[tax]['subtotal'], 2))
                        SubElement(ttimpuesto, "valor").text = str(round(taxes[tax]['iva'], 2))
                        if round(taxes[tax]['iva'], 2) == 0:
                            cr.execute(
                                "update account_invoice_ext set subtotal_0=%s where id=%s",
                                (round(taxes[tax]['subtotal'], 2), tuple([invoice.id]))
                                )
                            cr.commit()
                        else:
                            cr.execute(
                                "update account_invoice_ext set iva=%s ,subtotal_12=%s where id=%s",
                                (round(taxes[tax]['iva'], 2), round(taxes[tax]['subtotal'], 2), tuple([invoice.id]))
                                )
                            cr.commit()
                else:
                    cr.execute('delete from account_invoice_ext where id = %s', (tuple([str(invoice.id)])))
                    cr.commit()
                    return False
            if type_document == "factura":
                SubElement(factura, "propina").text = "0.0"
                SubElement(factura, "importeTotal").text = str(round(invoice.importetotal, 2))
                SubElement(factura, "moneda").text = "DOLAR"
                if invoice.way_pay_ids:
                    pagos = SubElement(factura, "pagos")
                    for way_pay in invoice.way_pay_ids:
                        pago = SubElement(pagos, "pago")
                        SubElement(pago, "formaPago").text = str(way_pay.forma_pago_id.codigo)
                        SubElement(pago, "total").text = str(way_pay.monto)
                        if way_pay.plazo:
                            SubElement(pago, "plazo").text = str(way_pay.plazo).strip()
                        if way_pay.unidad:
                            SubElement(pago, "unidadTiempo").text = str(way_pay.unidad).strip()
            if type_document == "notaCredito":
                SubElement(factura, "motivo").text = invoice.motivo.encode('ascii', 'ignore')
            if type_document in ("notaCredito", "factura"):
                detalles = SubElement(root, "detalles")
                if invoice.invoice_line_ext:
                    for line in invoice.invoice_line_ext:
                        dttline = SubElement(detalles, "detalle")
                        if type_document == "factura":
                            cPrincipal = line.codigoprincipal
                            if len(line.codigoprincipal) > 25:
                                cPrincipal = line.codigoprincipal[0:25]
                            SubElement(dttline, "codigoPrincipal").text = cPrincipal.encode('ascii', 'ignore')
                        elif type_document == "notaCredito":
                            cPrincipal = line.codigointerno
                            if len(line.codigointerno) > 25:
                                cPrincipal = line.codigointerno[0:25]
                            SubElement(dttline, "codigoInterno").text = cPrincipal.encode('ascii', 'ignore')
                        SubElement(dttline, "descripcion").text = line.descripcion.encode('ascii', 'ignore')
                        SubElement(dttline, "cantidad").text = str(line.cantidad)
                        SubElement(dttline, "precioUnitario").text = str(round(line.preciounitario, 2))
                        SubElement(dttline, "descuento").text = str(line.descuento)
                        SubElement(dttline, "precioTotalSinImpuesto").text = str(round(line.preciototalsinimpuesto, 2))
                        detalle_impuestos = SubElement(dttline, "impuestos")
                        dtle_impuesto = SubElement(detalle_impuestos, "impuesto")
                        SubElement(dtle_impuesto, "codigo").text = '2'  # impuesto iva
                        SubElement(dtle_impuesto, "codigoPorcentaje").text = line.codigoporcentaje
                        SubElement(dtle_impuesto, "tarifa").text = str(
                            self._get_value_iva(cr, uid, line.codigoporcentaje, context)
                            )
                        SubElement(dtle_impuesto, "baseImponible").text = str(round(line.preciototalsinimpuesto, 2))
                        SubElement(dtle_impuesto, "valor").text = str(
                            round(
                                line.preciototalsinimpuesto * self._get_value_iva(
                                    cr, uid, line.codigoporcentaje,
                                    context
                                    ) / 100.0, 2
                                )
                            )

            if type_document == "factura" and invoice.is_reemb:
                reembolsos = SubElement(root, "reembolsos")
                if invoice.invoice_line_reembolso_ext:
                    for reembi in invoice.invoice_line_reembolso_ext:
                        dttreembolso = SubElement(reembolsos, "reembolsoDetalle")
                        SubElement(
                            dttreembolso,
                            "tipoIdentificacionProveedorReembolso"
                            ).text = reembi.tipoidentificacionproveedorreembolso
                        SubElement(
                            dttreembolso,
                            "identificacionProveedorReembolso"
                            ).text = reembi.identificacionproveedorreembolso
                        SubElement(
                            dttreembolso,
                            "codPaisPagoProveedorReembolso"
                            ).text = reembi.codpaispagoproveedorreembolso
                        SubElement(dttreembolso, "tipoProveedorReembolso").text = reembi.tipoproveedorreembolso
                        SubElement(dttreembolso, "codDocReembolso").text = reembi.coddocreembolso if float(
                            reembi.baseimponiblereembolso
                            ) > 0 else "04"
                        SubElement(dttreembolso, "estabDocReembolso").text = reembi.estabdocreembolso
                        SubElement(dttreembolso, "ptoEmiDocReembolso").text = reembi.ptoemidocreembolso
                        SubElement(dttreembolso, "secuencialDocReembolso").text = reembi.secuencialdocreembolso
                        SubElement(dttreembolso, "fechaEmisionDocReembolso").text = reembi.fechaemisiondocreembolso
                        SubElement(dttreembolso, "numeroautorizacionDocReemb").text = reembi.numeroautorizaciondocreemb
                        detailimpuestosreembolso = SubElement(dttreembolso, "detalleImpuestos")
                        dtle_impuestoreembolso = SubElement(detailimpuestosreembolso, "detalleImpuesto")
                        SubElement(dtle_impuestoreembolso, "codigo").text = reembi.codigo
                        SubElement(dtle_impuestoreembolso, "codigoPorcentaje").text = reembi.codigoporcentaje
                        SubElement(dtle_impuestoreembolso, "tarifa").text = reembi.tarifa
                        SubElement(dtle_impuestoreembolso, "baseImponibleReembolso").text = str(
                            abs(round(float(reembi.baseimponiblereembolso), 2))
                            )
                        SubElement(dtle_impuestoreembolso, "impuestoReembolso").text = str(
                            abs(round(float(reembi.impuestoreembolso), 2))
                            )

            if type_document == "notaDebito":
                iva = int((invoice.importetotal / invoice.totalsinimpuestos - 1) * 100)
                if iva == 12:
                    cp = "2"
                elif iva == 13:
                    cp = "10"
                elif iva == 5:
                    cp = "5"
                elif iva == 15:
                    cp = "4"
                valor_iva = round(invoice.totalsinimpuestos * iva / 100.0, 2)
                detalle_impuestos = SubElement(factura, "impuestos")
                dtle_impuesto = SubElement(detalle_impuestos, "impuesto")
                SubElement(dtle_impuesto, "codigo").text = '2'  # impuesto iva
                SubElement(dtle_impuesto, "codigoPorcentaje").text = cp
                SubElement(dtle_impuesto, "tarifa").text = str(iva)
                SubElement(dtle_impuesto, "baseImponible").text = str(round(invoice.totalsinimpuestos, 2))
                SubElement(dtle_impuesto, "valor").text = str(valor_iva)
                SubElement(factura, "valorTotal").text = str(round(invoice.importetotal, 2))
                detalle_movitos = SubElement(root, "motivos")
                dtle_motivo = SubElement(detalle_movitos, "motivo")
                SubElement(dtle_motivo, "razon").text = invoice.motivo.encode('ascii', 'ignore')
                SubElement(dtle_motivo, "valor").text = str(round(invoice.totalsinimpuestos, 2))
                cr.execute(
                    "update account_invoice_ext set iva=%s , subtotal_12=%s where id=%s",
                    (iva, invoice.totalsinimpuestos, tuple([invoice.id]))
                    )
                cr.commit()
            self.indent(root)
            return tostring(root, encoding="UTF-8")
        # XML Comprobante retención
        elif type_document == "comprobanteRetencion":
            retention = docu_elect.browse(cr, uid, ids[0])
            est, pto, sec = retention.number.split("-")
            anuario, mes, dia = retention.fechaemision.split("-")
            clave_acceso = retention.clave_acceso
            root = Element(type_document)
            tributaria = SubElement(root, "infoTributaria")
            SubElement(tributaria, "ambiente").text = produccion
            SubElement(tributaria, "tipoEmision").text = emision
            SubElement(tributaria, "razonSocial").text = partner_company['reason_social'].encode('ascii', 'ignore')
            SubElement(tributaria, "ruc").text = partner_company['ruc']
            SubElement(tributaria, "claveAcceso").text = clave_acceso
            SubElement(tributaria, "codDoc").text = compr
            SubElement(tributaria, "estab").text = est
            SubElement(tributaria, "ptoEmi").text = pto
            SubElement(tributaria, "secuencial").text = sec
            SubElement(tributaria, "dirMatriz").text = partner_company['street'].encode('ascii', 'ignore')
            SubElement(tributaria, "agenteRetencion").text = "1"
            retencion = SubElement(root, "infoCompRetencion")
            SubElement(retencion, "fechaEmision").text = dia + "/" + mes + "/" + anuario
            SubElement(retencion, "dirEstablecimiento").text = retention.partner_id.street.encode('ascii', 'ignore')
            SubElement(retencion, "obligadoContabilidad").text = "SI"
            SubElement(retencion, "tipoIdentificacionSujetoRetenido").text = retention.partner_id.type_ref
            SubElement(retencion, "parteRel").text = "NO"
            SubElement(retencion, "razonSocialSujetoRetenido").text = retention.partner_id.name.encode(
                'ascii', 'ignore'
                )
            SubElement(retencion, "identificacionSujetoRetenido").text = retention.partner_id.vat
            SubElement(retencion, "periodoFiscal").text = retention.periodofiscal

            docs_sustento = SubElement(root, "docsSustento")
            doc_sustento = SubElement(docs_sustento, "docSustento")

            SubElement(doc_sustento, "codSustento").text = retention.cod_sustento
            SubElement(doc_sustento, "codDocSustento").text = retention.cod_doc_sustento
            SubElement(doc_sustento, "numDocSustento").text = retention.num_doc_sustento

            SubElement(doc_sustento, "fechaEmisionDocSustento").text = retention.fecha_emision_doc_sustento
            SubElement(doc_sustento, "pagoLocExt").text = retention.pago_loc and retention.pago_loc.zfill(2) or "01"
            if int(retention.pago_loc or 1) == 2:
                SubElement(doc_sustento, "tipoRegi").text = retention.tipo_regi
                SubElement(doc_sustento, "paisEfecPago").text = retention.pais_efec_pago
                SubElement(doc_sustento, "aplicConvDobTrib").text = retention.aplic_conv
                SubElement(doc_sustento, "pagoRegFis").text = retention.pago_reg_fis
            SubElement(doc_sustento, "totalSinImpuestos").text = "{:.2f}".format(retention.total_sin_impuesto)
            SubElement(doc_sustento, "importeTotal").text = "{:.2f}".format(retention.importe_total)
            impuestos_doc_sustento = SubElement(doc_sustento, "impuestosDocSustento")

            if retention.base_0:
                impuesto_doc_sustento = SubElement(impuestos_doc_sustento, "impuestoDocSustento")
                SubElement(impuesto_doc_sustento, "codImpuestoDocSustento").text = "2"
                SubElement(impuesto_doc_sustento, "codigoPorcentaje").text = "0"
                SubElement(impuesto_doc_sustento, "baseImponible").text = "{:.2f}".format(float(retention.base_0))
                SubElement(impuesto_doc_sustento, "tarifa").text = "0"
                SubElement(impuesto_doc_sustento, "valorImpuesto").text = "0"
            if retention.base_iva:
                impuesto_doc_sustento = SubElement(impuestos_doc_sustento, "impuestoDocSustento")
                SubElement(impuesto_doc_sustento, "codImpuestoDocSustento").text = "2"
                SubElement(impuesto_doc_sustento, "codigoPorcentaje").text = retention.codigo_porcentaje_iva
                SubElement(impuesto_doc_sustento, "baseImponible").text = "{:.2f}".format(
                    float(retention.base_iva)
                    )
                SubElement(impuesto_doc_sustento, "tarifa").text = retention.porcentaje_iva
                SubElement(impuesto_doc_sustento, "valorImpuesto").text = "{:.2f}".format(retention.iva)

            if retention.base_iva_5:
                impuesto_doc_sustento = SubElement(impuestos_doc_sustento, "impuestoDocSustento")
                SubElement(impuesto_doc_sustento, "codImpuestoDocSustento").text = "2"
                SubElement(impuesto_doc_sustento, "codigoPorcentaje").text = "5"
                SubElement(impuesto_doc_sustento, "baseImponible").text = "{:.2f}".format(
                    float(retention.base_iva_5)
                    )
                SubElement(impuesto_doc_sustento, "tarifa").text = "5"
                SubElement(impuesto_doc_sustento, "valorImpuesto").text = "{:.2f}".format(retention.base_iva_5 * 0.05)

            retenciones = SubElement(doc_sustento, "retenciones")
            if retention.retention_line_ext:
                for ltaxes in retention.retention_line_ext:
                    impuesto = SubElement(retenciones, "retencion")
                    SubElement(impuesto, "codigo").text = ltaxes.codigo
                    SubElement(impuesto, "codigoRetencion").text = ltaxes.codigoretencion
                    SubElement(impuesto, "baseImponible").text = "{:.2f}".format(ltaxes.baseimponible)
                    SubElement(impuesto, "porcentajeRetener").text = "{:.2f}".format(ltaxes.porcentajeretener)
                    SubElement(impuesto, "valorRetenido").text = "{:.2f}".format(ltaxes.valorretenido)

            pagos = SubElement(doc_sustento, "pagos")
            impuesto = SubElement(pagos, "pago")
            SubElement(impuesto, "formaPago").text = retention.codigo_pago
            SubElement(impuesto, "total").text = "{:.2f}".format(retention.importe_total)
            if retention.note:
                adicional = SubElement(root, "infoAdicional")
                for i, n in enumerate(retention.note.split("\n")):
                    SubElement(adicional, "campoAdicional", nombre="note%s" % (i + 1)).text = n.replace(
                        "-->", ""
                        ).encode(
                        'ascii', 'ignore'
                        )
            self.indent(root, context={'version': '2.0.0'})
            return tostring(root, encoding="UTF-8")
        else:
            remission = docu_elect.browse(cr, uid, ids[0])
            est, pto, sec = remission.number.split("-")
            anuario, mes, dia = remission.date_start_transport.split("-")
            clave_acceso = remission.clave_acceso
            root = Element(type_document)
            tributaria = SubElement(root, "infoTributaria")
            SubElement(tributaria, "ambiente").text = produccion
            SubElement(tributaria, "tipoEmision").text = emision
            SubElement(tributaria, "razonSocial").text = partner_company['reason_social'].encode('ascii', 'ignore')
            SubElement(tributaria, "ruc").text = partner_company['ruc']
            SubElement(tributaria, "claveAcceso").text = clave_acceso
            SubElement(tributaria, "codDoc").text = compr
            SubElement(tributaria, "estab").text = est
            SubElement(tributaria, "ptoEmi").text = pto
            SubElement(tributaria, "secuencial").text = sec
            SubElement(tributaria, "dirMatriz").text = partner_company['street'].encode('ascii', 'ignore')
            SubElement(tributaria, "agenteRetencion").text = "1"
            retencion = SubElement(root, "infoGuiaRemision")
            SubElement(retencion, "dirPartida").text = remission.street_start.encode('ascii', 'ignore')
            SubElement(retencion, "razonSocialTransportista").text = remission.reason_social_carrier.encode('ascii',
                                                                                                            'ignore')
            SubElement(retencion, "obligadoContabilidad").text = "SI"
            SubElement(retencion, "tipoIdentificacionTransportista").text = remission.type_ref_transp
            SubElement(retencion, "rucTransportista").text = remission.ruc_carrier
            SubElement(retencion, "fechaIniTransporte").text = self.get_date_format(
                remission.date_start_transport[:10].split("-")
                )
            SubElement(retencion, "fechaFinTransporte").text = self.get_date_format(
                remission.date_end_transport[:10].split("-")
                )
            SubElement(retencion, "placa").text = remission.plate
            destinatarios = SubElement(root, "destinatarios")
            destinatario = SubElement(destinatarios, "destinatario")
            SubElement(destinatario, "identificacionDestinatario").text = remission.partner_id.vat
            SubElement(destinatario, "razonSocialDestinatario").text = remission.partner_id.name.encode(
                'ascii',
                'ignore'
                )
            SubElement(destinatario, "dirDestinatario").text = remission.street_destination.encode('ascii', 'ignore')
            SubElement(destinatario, "motivoTraslado").text = remission.motive_transfer.encode('ascii', 'ignore')
            if remission.remission_line_ext:
                detalles = SubElement(destinatario, "detalles")
                for remi_line in remission.remission_line_ext:
                    detalle = SubElement(detalles, "detalle")
                    SubElement(detalle, "codigoInterno").text = remi_line.codigointerno.encode('ascii', 'ignore')
                    SubElement(detalle, "descripcion").text = remi_line.descripcion.encode('ascii', 'ignore')
                    SubElement(detalle, "cantidad").text = str(remi_line.cantidad)
                self.indent(root)
                return tostring(root, encoding="UTF-8")

            else:
                cr.execute('delete from remission_guide_ext where id = %s', (tuple([str(remission.id)])))
                cr.commit()
                return False

    ''' CREACION DE DATOS PARA LA FACTURACION ELECTRONICA'''

    def create_partner_data(self, cr, uid, informacion_partner):
        partner_obj = self.pool.get('res.partner')
        partner = None
        cr.execute(''' select id from res_partner where vat=%s''', (informacion_partner['ref'],))
        partners = cr.fetchone()
        mail_mail = ''
        mail_exist = informacion_partner['email'].find('@')
        if mail_exist != -1:
            mail_mail = informacion_partner['email']
        if informacion_partner['ref']:
            if partners:
                part = partner_obj.read(cr, uid, partners[0], ['id', 'email', 'street', 'name'])
                if informacion_partner['name'] != part['name']:
                    cr.execute(''' update res_partner set name=%s where id=%s''',
                               (informacion_partner['name'], part['id']))
                if mail_mail != part['email']:  # and "@" in (informacion_partner['email']):
                    cr.execute(''' update res_partner set email=%s where id=%s''', (mail_mail, part['id']))
                if informacion_partner['street'] != part['street']:
                    cr.execute(''' update res_partner set street=%s where id=%s''',
                               (informacion_partner['street'], part['id']))
                partner = part['id']
            else:
                query_insert = ''' insert into res_partner(name, active, create_system,vat, type_ref, street, email, is_company, notification_email_send, customer, create_date, create_uid, company_id, sent_invitation) values '''
                query_values = '''(%s, %s, %s, %s,%s, %s, %s, %s, %s, %s ,%s , %s, %s, %s)'''
                query_args = []
                query_args += (
                    informacion_partner['name'].encode('ascii', 'ignore'),
                    True,
                    True,
                    informacion_partner['ref'],
                    informacion_partner['type_ref'] or '',
                    informacion_partner['street'].encode('ascii', 'ignore') or '',
                    mail_mail,
                    True,
                    False,
                    True,
                    create_date,
                    uid,
                    self.pool.get('res.users').browse(cr, uid, uid, context=None).company_id.id,
                    False
                )

                cr.execute(query_insert + query_values, tuple(query_args))
                cr.commit()
                cr.execute('''select id from res_partner where vat=%s''', (informacion_partner['ref'],))
                partners = cr.fetchone()
                if partners:
                    partner = int(''.join(map(str, partners)))
        return partner

    def create_remission_guide_data(self, cr, uid, informacion, detalle, productos, number):
        partner = self.create_partner_data(cr, uid, detalle)
        cr.execute("select id from remission_guide_ext where number=%s", (number,))
        if not cr.fetchone():
            if partner:
                query_insert = ''' insert into remission_guide_ext(number, sent, count, street_start, partner_id, reason_social_carrier, type_ref_transp,
                                   ruc_carrier, date_start_transport, date_end_transport, plate,
                                   street_destination, motive_transfer, state, partner_name, ruc_partner, create_date, create_uid, company_id, clave_acceso, electronic_authorization) VALUES'''
                query_values = '(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)'
                clave_acceso = self.generate_new_access_key(cr, uid, number, informacion['date_start_transport'],
                                                            'remission.guide.ext', '06')
                query_args = []
                query_args += (
                    number,
                    False,
                    0,
                    informacion['street_start'].encode('ascii', 'ignore'),
                    partner,
                    informacion['reason_social_carrier'].encode('ascii', 'ignore'),
                    informacion['type_ref'],
                    informacion['ruc_carrier'],
                    datetime.strptime(informacion['date_start_transport'], "%d/%m/%Y"),
                    datetime.strptime(informacion['date_end_transport'], "%d/%m/%Y"),
                    informacion['plate'],
                    detalle['street'].encode('ascii', 'ignore'),
                    detalle['motive_transfer'].encode('ascii', 'ignore'),
                    'loaded',
                    detalle['name'].encode('ascii', 'ignore'),
                    detalle['ref'],
                    create_date,
                    uid,
                    self.pool.get('res.users').browse(cr, uid, uid, context=None).company_id.id,
                    clave_acceso,
                    clave_acceso
                )
                cr.execute(query_insert + query_values, tuple(query_args))
                cr.commit()
                cr.execute('select id from remission_guide_ext where number=%s', (number,))
                remission = cr.fetchone()[0]
                if productos:
                    query_insert = '''insert into remission_guide_line_ext (
                                                    codigointerno, remission_ext_id, descripcion, cantidad, create_date, create_uid) VALUES'''
                    query_values = ''
                    query_args = []
                    for prod in productos.keys():
                        if query_values:
                            query_values += ','
                        query_values += '(%s, %s, %s, %s, %s, %s)'
                        query_args += (
                            productos[prod].get('codigoInterno').encode('ascii', 'ignore'),
                            remission,
                            productos[prod].get('descripcion').encode('ascii', 'ignore'),
                            int(float(productos[prod].get('cantidad'))),
                            create_date,
                            uid
                        )

                    cr.execute(query_insert + query_values, tuple(query_args))
                    cr.commit()
                elif remission:
                    cr.execute('delete from remission_guide_ext where id=%s', (remission,))
                    cr.commit()

    def create_debit_note_data(self, cr, uid, informacion, number):
        partner = self.create_partner_data(cr, uid, informacion)
        cr.execute("select id from account_invoice_ext where number=%s and type='ndebit'", (number,))
        if not cr.fetchone():
            if partner:
                query_insert = ''' insert into account_invoice_ext(number, state, fechaemision, partner_id, type, sent, numdocmodificado,
                                   fechaemisiondocsustento, motivo, totalsinimpuestos, valormodificacion, importetotal, partner_name, 
                                   partner_email, direstablecimiento, identificacioncomprador, type_ref , create_date, create_uid, company_id, clave_acceso, electronic_authorization) VALUES'''
                query_values = '(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)'
                clave_acceso = self.generate_new_access_key(cr, uid, number, informacion['fechaEmision'],
                                                            'account.invoice.ext', '05')
                query_args = []
                query_args += (
                    number,
                    'loaded',
                    datetime.strptime(informacion['fechaEmision'], "%d/%m/%Y"),
                    partner,
                    'ndebit',
                    False,
                    informacion['numDocModificado'],
                    datetime.strptime(informacion['fechaEmisionDocSustento'], "%d/%m/%Y"),
                    informacion['motivo'] or '',
                    float(informacion['totalSinImpuestos']),
                    float(informacion['valorModificado']),
                    float(informacion['importeTotal']),
                    informacion['name'].encode('ascii', 'ignore'),
                    informacion['email'] or '',
                    informacion['street'].encode('ascii', 'ignore') or '',
                    informacion['ref'],
                    informacion['type_ref'] or '00',
                    create_date,
                    uid,
                    self.pool.get('res.users').browse(cr, uid, uid, context=None).company_id.id,
                    clave_acceso,
                    clave_acceso
                )
                cr.execute(query_insert + query_values, tuple(query_args))
                cr.commit()

    # def _get_iva(self, cr, uid, context=None):
    #    iva = 14
    #    return iva

    def _get_value_iva(self, cr, uid, code, context=None):
        value_iva = 0.00
        if code == '2':
            value_iva = 12.00
        elif code == '3':
            value_iva = 14.00
        elif code == '4':
            value_iva = 15
        elif code == '5':
            value_iva = 5
        elif code == '10':
            value_iva = 13
        return value_iva

    def create_credit_note_data(self, cr, uid, informacion, productos, number, edit_iva):
        partner = self.create_partner_data(cr, uid, informacion)
        cr.execute("select id from account_invoice_ext where number=%s and type='ncredito'", (number,))
        if not cr.fetchone():
            if partner:
                query_insert = ''' insert into account_invoice_ext(number, fechaemision, partner_id, type, sent, count,  numdocmodificado,
                                   fechaemisiondocsustento, motivo,totalsinimpuestos, importetotal, state, edit_iva,
                                   partner_name, partner_email, direstablecimiento, identificacioncomprador, type_ref ,create_date, create_uid, company_id, clave_acceso, electronic_authorization) VALUES'''
                query_values = '(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)'
                clave_acceso = self.generate_new_access_key(cr, uid, number, informacion['fechaEmision'],
                                                            'account.invoice.ext', '04')
                query_args = []
                query_args += (
                    number,
                    datetime.strptime(informacion['fechaEmision'], "%d/%m/%Y"),
                    partner,
                    'ncredito',
                    False,
                    0,
                    informacion['numDocModificado'],
                    datetime.strptime(informacion['fechaEmisionDocSustento'], "%d/%m/%Y"),
                    informacion['motivo'].encode('ascii', 'ignore'),
                    float(informacion['totalSinImpuestos']),
                    float(informacion['importeTotal']),
                    'loaded',
                    edit_iva,
                    informacion['name'].encode('ascii', 'ignore'),
                    informacion['email'],
                    informacion['street'].encode('ascii', 'ignore'),
                    informacion['ref'],
                    informacion['type_ref'],
                    create_date,
                    uid,
                    self.pool.get('res.users').browse(cr, uid, uid, context=None).company_id.id,
                    clave_acceso,
                    clave_acceso
                )
                cr.execute(query_insert + query_values, tuple(query_args))
                cr.commit()
                cr.execute("select id from account_invoice_ext where type='ncredito' and number=%s", (number,))
                invoice = cr.fetchone()[0]
                if productos:
                    query_insert = '''insert into account_invoice_line_ext(codigoprincipal,codigointerno,
                                          invoice_ext_id, descripcion, cantidad,
                                          preciounitario, descuento, preciototalsinimpuesto ,codigoporcentaje ,create_date, create_uid) VALUES'''
                    query_values = ''
                    query_args = []
                    for prod in productos.keys():
                        if query_values:
                            query_values += ','
                        query_values += '(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)'
                        query_args += (
                            productos[prod].get('codigoInterno').encode('ascii', 'ignore'),
                            productos[prod].get('codigoInterno').encode('ascii', 'ignore'),
                            invoice,
                            productos[prod].get('descripcion').encode('ascii', 'ignore'),
                            int(float(productos[prod].get('cantidad'))),
                            float(productos[prod].get('precioUnitario')),
                            float(productos[prod].get('descuento')),
                            float(productos[prod].get('precioTotalSinImpuesto')),
                            productos[prod].get('codigoPorcentaje'),
                            create_date,
                            uid
                        )
                    cr.execute(query_insert + query_values, tuple(query_args))
                    cr.commit()
                elif invoice:
                    cr.execute('delete from account_invoice_ext where id=%s', (invoice,))
                    cr.commit()

    def create_retention_data(self, cr, uid, informacion, detail, number):
        partner = self.create_partner_data(cr, uid, informacion)
        cr.execute("select id from account_retention_ext where number=%s", (number,))
        if not cr.fetchone():
            if partner:
                query_insert = '''insert into account_retention_ext(number, sent, count,  fechaemision, partner_id, 
                periodofiscal, state, 
                                             partner_name, partner_email, direstablecimiento, 
                                             identificacionsujetoretenido, type_ref, create_date, 
                                             create_uid, company_id, clave_acceso, electronic_authorization, 
                                             cod_sustento, cod_doc_sustento, num_doc_sustento, 
                                             fecha_emision_doc_sustento, pago_loc, pago_reg_fis, tipo_regi, 
                                             pais_efec_pago,
                                             aplic_conv, total_sin_impuesto, importe_total, base_0, base_iva, iva, 
                                             codigo_pago, porcentaje_iva, codigo_porcentaje_iva, base_iva_5
                                             ) VALUES'''
                query_values = '(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, ' \
                               '%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)'
                clave_acceso = self.generate_new_access_key(
                    cr, uid, number, informacion['fechaEmision'], 'account.retention.ext', '07'
                    )
                query_args = []
                query_args += (
                    number,
                    False,
                    0,
                    datetime.strptime(informacion['fechaEmision'], "%d/%m/%Y"),
                    partner,
                    informacion['periodoFiscal'] or '',
                    'loaded',
                    informacion['name'].encode('ascii', 'ignore'),
                    informacion['email'] or '',
                    informacion['street'].encode('ascii', 'ignore') or 'N/A',
                    informacion['ref'],
                    informacion['type_ref'],
                    create_date,
                    uid,
                    self.pool.get('res.users').browse(cr, uid, uid, context=None).company_id.id,
                    clave_acceso,
                    clave_acceso,
                    informacion['cod_sustento'],
                    informacion['cod_doc_sustento'],
                    informacion['num_doc_sustento'],
                    informacion['fecha_emision_doc_sustento'],
                    informacion['pago_loc'],
                    informacion['pago_reg_fis'],
                    informacion['tipo_regi'],
                    informacion['pais_efec_pago'],
                    informacion['aplic_conv'],
                    informacion['total_sin_impuesto'],
                    float(informacion['base_0']) + float(informacion['base_iva']) + float(informacion['iva']),
                    informacion['base_0'],
                    informacion['base_iva'],
                    informacion['iva'],
                    informacion['codigo_pago'],
                    informacion['porcentaje_iva'],
                    informacion['codigo_porcentaje_iva'],
                    informacion['base_iva_5'],
                    )
                cr.execute(query_insert + query_values, tuple(query_args))
                cr.commit()
                cr.execute('select id from account_retention_ext where number=%s', (number,))
                retention = cr.fetchone()[0]
                if retention:
                    query_insert = '''insert into account_retention_line_ext(
                    type_document, numdocsustento, tax, retention_ext_id, codigoretencion, baseimponible, 
                    porcentajeretener,
                    valorretenido ,codigo, create_date, create_uid, cod_sustento, fecha_doc_sustento) VALUES'''

                    query_values = ''
                    query_args = []
                    iva = False
                    for det in detail.keys():
                        if str(detail[det].get('codigo')) == '2':
                            iva = True
                        if query_values:
                            query_values += ','
                        query_values += '(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)'
                        query_args += (
                            detail[det].get('type_document'),
                            detail[det].get('numDocSustento'),
                            self.name_retention(str(detail[det].get('codigo'))),
                            retention,
                            detail[det].get('codigoRetencion'),
                            float(detail[det].get('baseImponible')),
                            float(detail[det].get('porcentajeRetener')),
                            float(detail[det].get('valorRetenido')),
                            detail[det].get('codigo'),
                            create_date,
                            uid,
                            detail[det].get('cod_sustento'),
                            datetime.strptime(detail[det].get('fecha_doc_sustento'), '%d/%m/%Y'),
                            )
                        # if iva is False:
                    #     query_values += ',(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)'
                    #     query_args += (
                    #                   detail[det].get('type_document'),
                    #                   detail[det].get('numDocSustento'),
                    #                   'IVA',
                    #                   retention,
                    #                   '7',
                    #                   0.00,
                    #                   0.00,
                    #                   0.00,
                    #                   '2',
                    #                   create_date,
                    #                   uid,
                    #                   detail[det].get('cod_sustento'),
                    #                   datetime.strptime(detail[det].get('fecha_doc_sustento'), '%d/%m/%Y'),
                    #                   )
                    cr.execute(query_insert + query_values, tuple(query_args))
                    cr.commit()
                elif detail:
                    cr.execute('delete from account_retention_ext where id=%s', (retention,))
                    cr.commit()

    def create_invoice_data(self, cr, uid, informacion, productos, number, guia, other_guides, edit_iva, reembolsoi,
                            reembolsop):
        partner = self.create_partner_data(cr, uid, informacion)
        cr.execute("select id from account_invoice_ext where number=%s and type='factura'", (number,))
        if not cr.fetchone():
            if partner:
                query_insert = ''' insert into account_invoice_ext(
                                   number, sent, count, guide_remision, other_guides, fechaemision, partner_id,
                                   type, totalsinimpuestos, totaldescuento, importetotal, state,partner_name,
                                   partner_email, direstablecimiento, identificacioncomprador, type_ref , create_date,
                                   create_uid, company_id, clave_acceso, electronic_authorization, edit_iva) VALUES'''
                query_values = '(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s ,%s, %s ,%s, %s)'
                clave_acceso = self.generate_new_access_key(cr, uid, number, informacion['fechaEmision'],
                                                            'account.invoice.ext', '01')
                query_args = []
                query_args += (
                    number,
                    False,
                    0,
                    guia or '',
                    other_guides or '',
                    datetime.strptime(informacion['fechaEmision'], '%d/%m/%Y'),
                    partner,
                    'factura',
                    informacion['totalSinImpuestos'],
                    informacion['totalDescuento'],
                    informacion['importeTotal'],
                    'loaded',
                    informacion['name'].encode('ascii', 'ignore'),
                    informacion['email'] or '',
                    informacion['street'].encode('ascii', 'ignore') or '',
                    informacion['ref'],
                    informacion['type_ref'] or '00',
                    create_date,
                    uid,
                    self.pool.get('res.users').browse(cr, uid, uid, context=None).company_id.id,
                    clave_acceso,
                    clave_acceso,
                    edit_iva,
                )
                cr.execute(query_insert + query_values, tuple(query_args))
                cr.commit()
                cr.execute("select id from account_invoice_ext where type='factura' and number=%s", (number,))
                invoice = cr.fetchone()
                for fp in informacion.get('fa', []):
                    cr.execute("select id from way_pay_text where codigo='%s'" % (
                        fp.get("codigo_forma_pago").strip().zfill(2),))
                    codigo = cr.fetchone()[0]
                    cr.execute("""insert into way_pay(forma_pago_id, monto, plazo, unidad, invoice_id)
                                  values (%s,%s,%s,%s, %s)""",
                               (codigo, float(fp.get("monto")), int(fp.get("plazo").strip() or 0),
                                fp.get("unidad"), invoice[0]))
                if productos:
                    query_insert = '''insert into account_invoice_line_ext(codigoprincipal, codigointerno,
                                          invoice_ext_id, descripcion, cantidad,
                                          preciounitario, descuento, preciototalsinimpuesto ,codigoporcentaje ,create_date, create_uid) VALUES'''

                    query_values = ''
                    query_args = []
                    for prod in productos.keys():
                        if query_values:
                            query_values += ','
                        query_values += '(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)'
                        query_args += (
                            productos[prod].get('codigoPrincipal').encode('ascii', 'ignore'),
                            productos[prod].get('codigoPrincipal').encode('ascii', 'ignore'),
                            invoice[0],
                            productos[prod].get('descripcion').encode('ascii', 'ignore'),
                            int(float(productos[prod].get('cantidad'))),
                            float(productos[prod].get('precioUnitario')),
                            float(productos[prod].get('descuento')),
                            float(productos[prod].get('precioTotalSinImpuesto')),
                            productos[prod].get('codigoPorcentaje') or None,
                            create_date,
                            uid
                        )
                    cr.execute(query_insert + query_values, tuple(query_args))
                    cr.commit()

                if reembolsoi:
                    for reembi in reembolsoi.keys():
                        cr.execute("""update account_invoice_ext set is_reemb=True, coddocReemb=%s, totalcomprobantesreembolso=%s,
                                    totalbaseimponiblereembolso=%s, totalimpuestoreembolso=%s
                                    where id=%s""", (reembolsoi[reembi].get('codDocReemb'),
                                                     reembolsoi[reembi].get('totalComprobantesReembolso'),
                                                     reembolsoi[reembi].get('totalBaseImponibleReembolso'),
                                                     reembolsoi[reembi].get('totalImpuestoReembolso'), invoice[0],))
                        cr.commit()

                if reembolsop:
                    query_redetail_insert = '''insert into account_invoice_line_reembolso_ext(invoice_ext_id, tipoidentificacionproveedorreembolso,
                                          identificacionproveedorreembolso, codpaispagoproveedorreembolso, tipoproveedorreembolso,
                                          coddocreembolso, estabdocreembolso, ptoEmidocreembolso, secuencialdocreembolso, fechaemisiondocreembolso,
                                          numeroautorizaciondocreemb, codigo, codigoporcentaje, tarifa, baseimponiblereembolso, impuestoreembolso,
                                          create_date, create_uid) VALUES'''

                    query_redetail_values = ''
                    query_redetail_args = []
                    for reembp in reembolsop.keys():
                        if query_redetail_values:
                            query_redetail_values += ','
                        query_redetail_values += '(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)'
                        query_redetail_args += (
                            invoice[0],
                            reembolsop[reembp].get('tipoIdentificacionProveedorReembolso'),
                            reembolsop[reembp].get('identificacionProveedorReembolso'),
                            reembolsop[reembp].get('codPaisPagoProveedorReembolso'),
                            reembolsop[reembp].get('tipoProveedorReembolso'),
                            reembolsop[reembp].get('codDocReembolso'),
                            reembolsop[reembp].get('estabDocReembolso'),
                            reembolsop[reembp].get('ptoEmiDocReembolso'),
                            reembolsop[reembp].get('secuencialDocReembolso'),
                            reembolsop[reembp].get('fechaEmisionDocReembolso'),
                            reembolsop[reembp].get('numeroautorizacionDocReemb'),
                            reembolsop[reembp].get('codigo'),
                            reembolsop[reembp].get('codigoPorcentaje'),
                            reembolsop[reembp].get('tarifa'),
                            reembolsop[reembp].get('baseImponibleReembolso'),
                            reembolsop[reembp].get('impuestoReembolso'),
                            create_date,
                            uid
                        )
                    cr.execute(query_redetail_insert + query_redetail_values, tuple(query_redetail_args))
                    cr.commit()

    ''' LECTURA DE ARCHIVOS PARA LA CREACION DE DATOS '''

    def read_file_upload_remission_guide(self, cr, uid, contenido, context=None):
        informacion, detalle, productos, number = {}, {}, {}, ""
        cuenta = 0
        company_ruc = self.pool.get('res.users').browse(cr, uid, uid).company_id.ruc
        try:
            for datos in contenido:
                if datos == " ":
                    continue
                info = datos.split(",")
                if info[0].lower() == "n":
                    if informacion:
                        self.create_remission_guide_data(cr, uid, informacion, detalle, productos, number)
                        informacion, productos, number, cuenta = {}, {}, 0, 0
                    number = info[1].strip()
                elif info[0].lower() == 'r':
                    if info[1].strip() != company_ruc:
                        raise osv.except_osv("Error",
                                             "El archivo que esta subiendo no corresponde a la empresa, favor verificar el ruc en el archivo")
                elif info[0].lower() == "i":
                    informacion = {
                        'street_start': info[1].strip(),
                        'reason_social_carrier': info[2].strip(),
                        'ruc_carrier': info[3].strip(),
                        'type_ref': info[4].strip(),
                        'date_start_transport': info[5].strip(),
                        'date_end_transport': info[6].strip(),
                        'plate': info[7].strip(),
                    }
                elif info[0].lower() == "d":
                    detalle = {
                        'ref': info[1].strip(),
                        'name': info[2].strip(),
                        'street': info[3].strip(),
                        'motive_transfer': info[4].strip(),
                        'email': info[5].strip() if len(info) >= 6 else "",
                        'type_ref': ""
                    }
                elif info[0].lower() == "p":
                    cuenta += 1
                    productos["producto" + str(cuenta)] = {
                        'codigoInterno': info[1].strip(),
                        'descripcion': info[2].strip(),
                        'cantidad': info[3],
                    }

            self.create_remission_guide_data(cr, uid, informacion, detalle, productos, number)
        except Exception as e:
            raise osv.except_osv('Error',
                                 u"Ocurrio un error al crear la Guía de Remisión No.%s\nFavor verificar el archivo\nLos documentos anteriores a %s\n fueron creados con exito \nDETALLE: %s" % (
                                 number, number, str(e)))

    def read_file_upload_debit_note(self, cr, uid, contenido, context=None):
        informacion, number = {}, ""
        company_ruc = self.pool.get('res.users').browse(cr, uid, uid).company_id.ruc
        try:
            for datos in contenido:
                info = datos.split(",")
                if info[0].lower() == "n":
                    if informacion:
                        self.create_debit_note_data(cr, uid, informacion, number)
                        informacion, number = {}, 0
                    number = info[1].strip()
                elif info[0].lower() == 'r':
                    if info[1].strip() != company_ruc:
                        raise osv.except_osv("Error",
                                             "El archivo que esta subiendo no corresponde a la empresa, favor verificar el ruc en el archivo")
                elif info[0].lower() == "i":
                    informacion = {
                        'fechaEmision': info[1].strip(),
                        'ref': info[2].strip(),
                        'type_ref': info[3].strip(),
                        'name': info[4].strip(),
                        'street': info[5].strip(),
                        'numDocModificado': info[6].strip(),
                        'fechaEmisionDocSustento': info[7].strip(),
                        'motivo': info[8].strip(),
                        'valorModificado': info[9],
                        'totalSinImpuestos': info[9],
                        'importeTotal': info[10],
                        'email': info[11].strip() if len(info) >= 12 else ""
                    }
            self.create_debit_note_data(cr, uid, informacion, number)
        except Exception as e:
            raise osv.except_osv('Error',
                                 u"Ocurrio un error al crear la Nota de Débito No.%s\nFavor verificar el archivo\n<strong>Los documentos anteriores a %s\n fueron creados con éxito\nDETALLE: %s" % (
                                 number, number, str(e)))

    def read_file_upload_credit_note(self, cr, uid, contenido, context=None):
        productos, informacion = {}, {}
        cuenta_product, number = 0, ""
        company_ruc = self.pool.get('res.users').browse(cr, uid, uid).company_id.ruc
        try:
            edit_iva = 12
            for datos in contenido:
                info = datos.split(",")
                if info[0].lower() == "n":
                    if informacion:
                        self.create_credit_note_data(cr, uid, informacion, productos, number, edit_iva)
                        informacion, productos, number = {}, {}, 0
                        cuenta_product = 0
                    number = info[1].strip()
                elif info[0].lower() == 'r':
                    if info[1].strip() != company_ruc:
                        raise osv.except_osv("Error",
                                             "El archivo que esta subiendo no corresponde a la empresa, favor verificar el ruc en el archivo")
                elif info[0].lower() == "i":
                    informacion = {
                        'fechaEmision': info[1].strip(),
                        'ref': info[2].strip(),
                        'type_ref': info[3].strip(),
                        'name': info[4].strip(),
                        'street': info[5].strip(),
                        'numDocModificado': info[6].strip(),
                        'fechaEmisionDocSustento': info[7].strip(),
                        'motivo': info[8].strip(),
                        'totalSinImpuestos': info[9],
                        'importeTotal': info[10],
                        'email': info[11].strip() if len(info) >= 12 else ""
                    }
                elif info[0].lower() == "p":
                    cuenta_product += 1
                    productos["producto" + str(cuenta_product)] = {
                        'codigoInterno': info[1].strip(),
                        'descripcion': info[2].strip(),
                        'cantidad': info[3],
                        'precioUnitario': info[4],
                        'descuento': info[5],
                        'precioTotalSinImpuesto': info[6],
                        'codigoPorcentaje': info[7].strip()
                    }
                    if info[7].strip() == '2':
                        edit_iva = 12
                    elif datetime.now().date() < datetime.strptime('01/06/2017', '%d/%m/%Y').date():
                        edit_iva = 14
            self.create_credit_note_data(cr, uid, informacion, productos, number, edit_iva)
        except Exception as e:
            raise osv.except_osv('Error',
                                 u"Ocurrio un error al crear la Nota de Crédito No.%s\nFavor verificar el archivo\n<strong>Los documentos anteriores a %s\n fueron creados con exito\nDETALLE: %s" % (
                                 number, number, str(e)))

    def read_file_upload_retention(self, cr, uid, contenido, context=None):
        detail, informacion = {}, {}
        cuenta_detail, number = 0, ""
        fecha_emision = ''
        company_ruc = self.pool.get('res.users').browse(cr, uid, uid).company_id.ruc
        try:
            for datos in contenido:
                info = datos.split(",")
                if info[0].lower() == "n":
                    if informacion:
                        self.create_retention_data(cr, uid, informacion, detail, number)
                        informacion, detail, number = {}, {}, 0
                        fecha_emision = ''
                        cuenta_detail = 0
                    number = info[1].strip()
                elif info[0].lower() == "i":
                    fecha_emision = info[1].strip()
                    informacion = {
                        'fechaEmision': fecha_emision,
                        'ref': info[2].strip(),
                        'type_ref': info[3].strip(),
                        'name': info[4].strip(),
                        'street': info[5].strip(),
                        'periodoFiscal': info[6].strip().zfill(7),
                        'email': info[7].strip() if len(info) >= 8 else "",
                        'cod_sustento': info[8].strip() if len(info) >= 9 else "",
                        'cod_doc_sustento': info[9].strip() if len(info) >= 10 else "",
                        # 'num_doc_sustento': info[10].strip() if len(info) >= 8 else "",
                        'fecha_emision_doc_sustento': info[10].strip() if len(info) >= 8 else "",
                        'pago_loc': info[11].strip() if len(info) >= 8 else "",
                        'pago_reg_fis': info[12].strip() if len(info) >= 8 else "",
                        'tipo_regi': info[13].strip() if len(info) >= 8 else "",
                        'pais_efec_pago': info[14].strip() if len(info) >= 8 else "",
                        'aplic_conv': info[15].strip() if len(info) >= 8 else "",
                        'total_sin_impuesto': info[16].strip() if len(info) >= 8 else "",
                        'importe_total': float(info[18].strip()) + float(info[19].strip()) + float(info[20].strip()),
                        'base_0': info[17].strip() if len(info) >= 8 else "",
                        'base_iva': info[18].strip() if len(info) >= 8 else "",
                        'base_iva_5': info[19].strip() if len(info) >= 8 else "",
                        'iva': info[20].strip() if len(info) >= 8 else "",
                        'codigo_pago': info[21].strip() if len(info) >= 8 else "",
                        'porcentaje_iva': info[22].strip() if len(info) >= 8 else "",
                        'codigo_porcentaje_iva': info[23].strip() if len(info) >= 8 else "",
                        }
                elif info[0].lower() == "r":
                    if info[1].strip() != company_ruc:
                        raise osv.except_osv(
                            "Error",
                            "El archivo que esta subiendo no corresponde a la empresa, favor verificar el ruc en el "
                            "archivo"
                            )
                elif info[0].lower() == "d":
                    cuenta_detail += 1
                    type_document = 'FACTURA'
                    if len(info) > 7:
                        cod_sustento = info[7].strip()
                        if cod_sustento == '03':
                            type_document = 'LIQ. COMPRA'
                        elif cod_sustento == '04':
                            type_document = 'NOTA DE CREDITO'
                        elif cod_sustento == '02':
                            type_document = 'NOTA o BOLETA DE VENTA'
                        elif cod_sustento == '05':
                            type_document = 'NOTA DE DEBITO'
                        elif cod_sustento == '11':
                            type_document = 'Pasajes expedidos por empresas de aviación'
                        elif cod_sustento == '08':
                            type_document = 'Boletos o entradas a espectáculos públicos'
                        elif cod_sustento == '09':
                            type_document = 'Tiquetes o vales emitidos por máquinas registradoras'
                        elif cod_sustento == '12':
                            type_document = 'Documentos emitidos por instituciones financieras'
                        elif cod_sustento == '15':
                            type_document = 'Comprobante de venta emitido en el Exterior'
                        elif cod_sustento == '12':
                            type_document = 'Documentos emitidos por instituciones financieras'
                        elif cod_sustento == '41':
                            type_document = 'Comprobante de venta emitido por reembolso'
                        elif cod_sustento == '18':
                            type_document = 'Documentos autorizados utilizados en ventas excepto N/C N/D'
                        elif cod_sustento == '19':
                            type_document = 'Comprobantes de Pago de Cuotas o Aportes'
                    detail["detalle" + str(cuenta_detail)] = {
                        'codigo': info[1].strip(),
                        'codigoRetencion': info[2].strip(),
                        'baseImponible': info[3],
                        'porcentajeRetener': info[4],
                        'valorRetenido': info[5],
                        'numDocSustento': info[6].strip(),
                        'cod_sustento': info[7].strip() if len(info) > 7 else '',
                        'fecha_doc_sustento': fecha_emision,
                        'type_document': type_document
                        }
                    informacion['num_doc_sustento'] = info[6].strip()
            self.create_retention_data(cr, uid, informacion, detail, number)
        except Exception as e:
            print
            str(e)
            raise osv.except_osv('Error',
                                 u"Ocurrio un error al crear la Retención No.%s\nFavor verificar el archivo\nLos documentos anteriores a %s\n fueron creados con exito\nDETALLE: %s" % (
                                 number, number, str(e))
                                 )

    def read_file_upload_invoice(self, cr, uid, contenido, context=None):
        productos, informacion, reembolsoi, reembolsop = {}, {}, {}, {}
        cuenta_product, reembi, reembp, number, guia, other_guides = 0, 0, 0, "", "", ""
        company_ruc = self.pool.get('res.users').browse(cr, uid, uid).company_id.ruc
        try:
            edit_iva = 12
            for datos in contenido:
                info = datos.split(",")
                if info[0].lower().strip("\r") == "n":
                    if informacion:
                        cr.execute("select id from account_invoice_ext where type='factura' and number=%s", (number,))
                        invoice_test = cr.fetchone()
                        if not invoice_test:
                            self.create_invoice_data(cr, uid, informacion, productos, number, guia, other_guides,
                                                     edit_iva, reembolsoi, reembolsop)
                            informacion, productos, number = {}, {}, 0
                            cuenta_product = 0
                    number = info[1].strip()
                    productos = {}
                    informacion['fa'] = []
                elif info[0].lower().strip("\r") == 'fa':
                    informacion['fa'].append({
                        'codigo_forma_pago': info[1],
                        'monto': info[2],
                        'plazo': info[3] if len(info) > 3 else '0',
                        'unidad': info[4] if len(info) > 4 else '',
                    })
                elif info[0].lower() == "r":
                    if info[1].strip() != company_ruc:
                        raise osv.except_osv("Error",
                                             "El archivo que esta subiendo no corresponde a la empresa, favor verificar el ruc en el archivo")
                elif info[0].lower().strip("\r") == "i":
                    informacion.update({
                        'fechaEmision': info[1].strip(),
                        'ref': info[2].strip(),
                        'type_ref': info[3].strip(),
                        'name': info[4].strip(),
                        'street': info[5].strip(),
                        'totalSinImpuestos': info[6],
                        'totalDescuento': info[7],
                        'importeTotal': info[8],
                        'email': info[9].strip() if len(info) >= 10 else ""
                    })
                elif info[0].lower().strip("\r") == "p":
                    cuenta_product += 1
                    productos["producto" + str(cuenta_product)] = {
                        'codigoPrincipal': info[1].strip(),
                        'descripcion': info[2].strip(),
                        'cantidad': info[3],
                        'precioUnitario': info[4],
                        'descuento': info[5],
                        'precioTotalSinImpuesto': info[6],
                        'codigoPorcentaje': info[7].strip()
                    }
                    if info[7].strip() == '2':
                        edit_iva = 12
                    elif datetime.now().date() < datetime.strptime('01/06/2017', '%d/%m/%Y').date():
                        edit_iva = 14
                elif info[0].lower().strip("\r") == "g":
                    guia = info[1]
                    other_guides = ", ".join(info[1:]) if len(info) > 1 else ""

                elif info[0].lower().strip("\r") == "ir":  # cabecera factura reembolso
                    reembi += 1
                    reembolsoi["reembolsoi" + str(reembi)] = {
                        'codDocReemb': '41',
                        'totalComprobantesReembolso': str(info[1].strip()),
                        'totalBaseImponibleReembolso': str(info[2].strip()),
                        'totalImpuestoReembolso': str(info[3].strip()),
                    }

                elif info[0].lower().strip("\r") == "pr":  # cabecera factura reembolso
                    reembp += 1
                    est, pto, sec = info[3].strip().split("-")
                    cod_por = info[8].strip()
                    tarifa = int(info[9].strip())
                    # if cod_por=='2':
                    #     tarifa=12
                    # elif other
                    # else: tarifa=0
                    reembolsop["reembolsop" + str(reembp)] = {
                        'tipoIdentificacionProveedorReembolso': str(info[1].strip()),
                        'identificacionProveedorReembolso': str(info[2].strip()),
                        'codPaisPagoProveedorReembolso': '593',
                        'tipoProveedorReembolso': '02',
                        'codDocReembolso': '01',
                        'estabDocReembolso': str(est),
                        'ptoEmiDocReembolso': str(pto),
                        'secuencialDocReembolso': str(sec),
                        'fechaEmisionDocReembolso': str(info[4].strip()),
                        'numeroautorizacionDocReemb': str(info[5].strip()),
                        'codigo': '2',
                        'codigoPorcentaje': str(cod_por),
                        'tarifa': str(tarifa),
                        'baseImponibleReembolso': str(info[6].strip()),
                        'impuestoReembolso': str(info[7].strip())
                    }

            cr.execute("select id from account_invoice_ext where type='factura' and number=%s", (number,))
            invoice_test = cr.fetchone()
            if not invoice_test: self.create_invoice_data(cr, uid, informacion, productos, number, guia, other_guides,
                                                          edit_iva, reembolsoi, reembolsop)
        except Exception as e:
            raise osv.except_osv('Error',
                                 "Ocurrio un error al crear la Factura No.%s\nFavor verificar el archivo\nLos documentos anteriores a %s\n fueron creados con exito\nDETALLE: %s" % (
                                 number, number, str(e)))

    ''' FIN DE LECTURA DE ARCHIVOS '''

    def __send_mail(self, cr, uid, document, model, email_template, context=None):
        template_obj = self.pool.get('email.template')
        ir_model_data = self.pool.get('ir.model.data')
        ir_mail_server = self.pool.get('ir.mail_server')
        attachment_obj = self.pool.get('ir.attachment')
        cr.execute('''select login, password from res_users where partner_id = %s''', (document['partner_id'],))
        credenciales = cr.fetchone()
        template_id = ir_model_data.get_object_reference(cr, uid, 'voucher_electronic', email_template)[1]
        template_email = template_obj.generate_email(cr, uid, template_id, document['id'],
                                                     context={'tz': False, 'active_model': model,
                                                              'type': document.get('type'),
                                                              'active_id': document['id']})
        attachments_content = []
        if template_email.get('attachment_ids') and credenciales:
            for attach in template_email.get('attachment_ids'):
                at = attachment_obj.browse(cr, uid, [attach])[0]
                attachments_content.append((at.datas_fname, base64.b64decode(at.datas)))
            msg = ir_mail_server.build_email(
                email_from=template_email.get("email_from"),
                email_to=[_('<%s>') % document['email']],
                subject=template_email.get('subject'),
                body=template_email.get('body') + _(
                    """<br />Las credenciales para acceder al portal son: <br /><br />login: <strong>%s</strong> <br /> password: <strong>%s</strong>""") % (
                     credenciales[0], credenciales[1]),
                body_alternative=None,
                email_cc=[template_email.get("email_cc")],
                reply_to=False,
                attachments=attachments_content,
                message_id=None,
                references=False,
                object_id=None,
                subtype='html',
                subtype_alternative='plain')
            res = ir_mail_server.send_email(cr, uid, msg,
                                            mail_server_id=None, context=context)
            if res:
                cr.execute("update " + model.replace(".", "_") + " set sent=True where id=%s", ([document['id']]))
                cr.commit()

    # ENVIO DE DOCUMENTOS POR CORREO ELECTRONICO MEDIANTE TAREA CRON
    def send_mail_documents(self, cr, uid, ids, context=None):
        if not context: context = {}
        try:
            self.create_user(cr, uid, ids, context=None)
        except Exception as e:
            pass

        cr.execute("""
              SELECT 
               a.id as id,
               a.partner_id as partner_id,
               a.type as type,
               r.email as email
               FROM account_invoice_ext as a
               left join res_partner r on (a.partner_id=r.id)
               WHERE a.state='authorized' and a.type='factura' and a.sent=False and r.email ILIKE '%@%'
             """)
        invoice_ids_factura = cr.dictfetchall()
        if invoice_ids_factura:
            for document in invoice_ids_factura:
                try:
                    self.__send_mail(cr, uid, document, 'account.invoice.ext', 'email_template_edi_account_invoice_ext')
                except Exception, e:
                    print
                    "not sent factura %s" % str(e)
                    continue

        # Guias de remisión
        cr.execute("""
                  SELECT 
                   g.id as id,
                   g.partner_id as partner_id,
                   r.email as email
                   FROM remission_guide_ext as g
                   left join res_partner r on (g.partner_id=r.id)
                   WHERE g.state='authorized' and g.sent=False and r.email ILIKE '%@%'
                 """)
        remission_ids = cr.dictfetchall()
        if remission_ids:
            for document in remission_ids:
                try:
                    self.__send_mail(cr, uid, document, 'remission.guide.ext', 'email_template_edi_remission_ext')
                except Exception, e:
                    print
                    "not sent guide remission %s" % str(e)
                    continue

        # Comprobantes de retención
        cr.execute("""
              SELECT 
               a.id as id,
               a.partner_id as partner_id,
               r.email as email
               FROM account_retention_ext as a
               left join res_partner r on (a.partner_id=r.id)
               WHERE a.state='authorized' and a.sent=False and r.email ILIKE '%@%'
             """)
        retention_ids = cr.dictfetchall()
        if retention_ids:
            for document in retention_ids:
                try:
                    self.__send_mail(cr, uid, document, 'account.retention.ext', 'email_template_edi_withhold_ext')
                except Exception, e:
                    print
                    "not sent retention %s" % str(e)
                    continue

        # Notas de crédito
        cr.execute("""
              SELECT 
               a.id as id,
               a.partner_id as partner_id,
               a.type as type,
               r.email as email
               FROM account_invoice_ext as a
               left join res_partner r on (a.partner_id=r.id)
               WHERE a.state='authorized' and a.type='ncredito' and a.sent=False and r.email ILIKE '%@%'
             """)
        invoice_ids_ncredito = cr.dictfetchall()
        if invoice_ids_ncredito:
            for document in invoice_ids_ncredito:
                try:
                    self.__send_mail(cr, uid, document, 'account.invoice.ext',
                                     'email_template_edi_account_invoice_nc_ext')
                except Exception, e:
                    print
                    "not sent note credit %s" % str(e)
                    continue

        # Notas de débito
        cr.execute("""
              SELECT 
               a.id as id,
               a.partner_id as partner_id,
               a.type as type,
               r.email as email
               FROM account_invoice_ext as a
               left join res_partner r on (a.partner_id=r.id)
               WHERE a.state='authorized' and a.type='ndebit' and a.sent=False and r.email ILIKE '%@%'
             """)
        invoice_ids_ndebit = cr.dictfetchall()
        if invoice_ids_ndebit:
            for document in invoice_ids_ndebit:
                try:
                    self.__send_mail(cr, uid, document, 'account.invoice.ext',
                                     'email_template_edi_account_invoice_nd_ext')
                except Exception, e:
                    print
                    "not sent note debit %s" % str(e)
                    continue
        return True

    def create_user(self, cr, uid, ids, context=None):
        cr.execute(
            "select id, vat, name from res_partner where active=True and customer=True and create_system=True and sent_invitation=False limit 50")
        registros = cr.dictfetchall()
        user_obj = self.pool.get('res.users')
        cr.execute('select id, name from res_company where partner_id=%s', (uid,))
        company = cr.fetchone()
        cr.execute("select id from res_groups where name='Portal'")
        gid = cr.fetchone()
        groups = [gid]
        for registro in registros:
            if registro['vat']:
                cr.execute('select id from res_users where login=%s or partner_id=%s',
                           (registro['vat'], registro['id']))
                if not cr.fetchone():
                    user_id = user_obj.create(cr, uid, {'active': True, 'login': registro['vat'],
                                                        'company_id': company[0],
                                                        'password': registro['vat'],
                                                        'partner_id': registro['id'],
                                                        'groups_id': [(6, 0, groups)],
                                                        'is_system_create': True,
                                                        'lang': 'es_ES'
                                                        })
                    if user_id:
                        self.pool.get('res.partner').write(cr, uid, [registro['id']], {'sent_invitation': True})


electronic_invoicing_sri()
