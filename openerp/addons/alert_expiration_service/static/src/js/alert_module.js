openerp.alert_expiration_service = function (instance) {

    instance.web.client_actions.add('cisc.alert', 'instance.alert_expiration_service.Action');

    instance.alert_expiration_service.Action = instance.web.Widget.extend({

        className: 'oe_alert_expiration_service',

        start: function () {
		new instance.web.Model('res.company').call('time_over', [0]).then(function(result){
			if(result !== false){
				var hoy = new Date();
				var dia_actual = parseInt(hoy.getDate());
				var dia_corte = parseInt(result[0]);
				var r = dia_corte - dia_actual;
				var result = r;
				if (result <= 0){
					alert("Estimado(a) cliente el sistema se encuentra inhabilitado, contactarse con el departamento de Ventas/Cobranzas.");
					new instance.web.Model('res.company').call('game_over', [0]);
				}
				else {
					alert("Estimado(a) cliente: El sistema se inactivará en "+result+ " días por falta de Pago. Para mayor información favor contactarse con el departamento de Ventas/Cobranzas");
				}
				
			}
		});
            return this._super();
        }
    });
};